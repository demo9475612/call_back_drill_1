const fs = require('fs');


function createRandomDirectoryAndFilesInIt(directoryPath,numberOfFiles,callbackfn){
 
        //creating directory using directroy path
        fs.mkdir(directoryPath,(error)=>{
            //if directory is already exist then show error else continue.
            if(error){
              callbackfn(error);
              return;
            } 

            //for creating file in directory using loop 
            for(let index=0;index<=numberOfFiles;index++){
                // file path shows the path of file where to create file
                const filePath = `${directoryPath}/file${index}.json`;
                //console.log(fileCreationPath);
                fs.writeFile(filePath,`This is file${index}`,(error)=>{
                  // if error in creating file then showing error using callback function.
                  if(error){
                     callbackfn(error);
                     return;
                  }
                  console.log('File created successfully',filePath)

                  // deleting file from the directory using unlink method
                  fs.unlink(filePath, (err) => {
                    if (err) {
                      console.error(err);
                      return;
                    }
                    console.log('File deleted successfully',filePath);
                  });
                });
                
            }
            // returning callback function if file created successfully and deleted successfully and passing error as null.
            callbackfn(null);
        });
    
}

module.exports = createRandomDirectoryAndFilesInIt;