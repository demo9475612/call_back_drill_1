const fs = require("fs");

function convertToUpperCaseAndWrite(inputFile,callback){

        fs.readFile(inputFile,'utf8',(error,inputData)=>{
            if(error){
                callback(error);
            }
            const upperCaseFile = `upperCase_file.txt`;
            const upperCaseContent = inputData.toUpperCase();

            fs.writeFile(upperCaseFile,upperCaseContent,(error)=>{
                    if(error){
                        callback(error);
                    }
                    fs.appendFile('filenames.txt', `${upperCaseFile}\n`, (err) => {
                        if (err) return callback(err);
                        console.log("UpperCase File is Created :",upperCaseFile);
                    });
            });
            fs.readFile(upperCaseFile,'utf8',(error,uperCaseData)=>{
                if(error){
                   callback(error);
                }
                const lowerCaseFile = `lowerCaseFile.txt`;
                const lowerCaseContent = uperCaseData.toLowerCase();
                const splitContent = lowerCaseContent.split('.');

                const tripSentence = splitContent.map(sentence=>sentence.trim()).join('.\n');
                fs.writeFile(lowerCaseFile,tripSentence,(error)=>{
                    if(error){
                        callback(error);
                    }
                });
                
                
                fs.appendFile('filenames.txt', `${lowerCaseFile}\n`, (err) => {
                    if (err) return callback(err);
                    console.log("LowerCase File is Created:",lowerCaseFile);
                });
                
                fs.readFile(lowerCaseFile,'utf8',(error,lowerCaseData)=>{
                    if(error){
                        callback(error);
                    }
                    const sortedFile = `lipsum_1_sorted.txt`;
                    const content = lowerCaseData.split('\n');
                    const sortContent = content.sort();
                    const sortedContent=sortContent.join('\n');
                    //console.log(sortedContent);

                    fs.writeFile(sortedFile,sortedContent,(error)=>{
                        if(error){
                            callback(error);
                        }
                    })
                    fs.appendFile('filenames.txt', `${sortedFile}`, (err) => {
                        if (err) return callback(err);
                        console.log("Sorted File is Created:",sortedFile);
                    });
                   
                    const fileNames = '/home/a/Desktop/Call_Back_Drill_1/test/filenames.txt';
                    
                    fs.readFile(fileNames,'utf8',(error,listOfFiles)=>{
                         if(error){
                            callback(error);
                            
                         }
                         
                         const fileList = listOfFiles.split('\n');
                         //console.log(fileList);
                         for(let index=0;index<fileList.length;index++){
                            
                            fs.unlink(fileList[index],(error)=>{
                                if(error){
                                    console.error(error);
                                }
                                else{
                                    console.log("Deleted File is:",fileList[index]);
                                }
                            })
                         }

                    });
                    
                });
            });
        });
}
module.exports =convertToUpperCaseAndWrite;